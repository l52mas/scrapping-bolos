# Python 3.11
# Author: Christian Badillo
# Date: 2024-16-04

# Importamos las librerias necesarias
import asyncio
from playwright.async_api import async_playwright, Playwright
import pandas as pd
import os

# La función sirve para obtener los links de las noticias de Newtral.es
async def get_links(playwright: Playwright) -> list[str]:
    """
    Interactua con la página de Newtral.es para obtener información de salud
    respecto al Coronavirus y con el filtro de "Fakes".
    """
    browser = await playwright.chromium.launch() # Abrimos el navegador
    page = await browser.new_page() # Abrimos una nueva pestaña
    await page.goto("https://www.newtral.es/zona-verificacion/fakes/", wait_until="networkidle") # Vamos a la página de Newtral.es y esperamos a que cargue
    await page.locator("ul.c-suggestions-pills").get_by_text("Coronavirus").click() # Hacemos click en el filtro de "Coronavirus"
    await page.get_by_role("combobox").nth(0).click() # Interactuamos con el filtro
    await page.get_by_role("option").nth(1).click() # Seleccionamos la opción "Falso"
    
    visible = True # Variable para saber si hay más noticias por cargar

    # Mientras haya más noticias por cargar, se cargan
    while visible:
        await page.get_by_role("button", name="Cargar más").click() # Hacemos click en el botón de "Cargar más"
        await page.wait_for_timeout(4000) # Esperamos a que carguen las noticias
        visible = await page.get_by_role("button", name="Cargar más").is_visible() # Verificamos si hay más noticias por cargar

    locator = await page.locator("a.quote_link.js-card").all() # Obtenemos los links de las noticias
    links = [await element.get_attribute("href") for element in locator] # Guardamos los links de las noticias

    await page.screenshot(path="newtral_es.png") # Tomamos una captura de pantalla para verificar que se cargaron todas las noticias
    await browser.close() # Cerramos el navegador

    # Retornamos los links de las noticias
    return links

# Definimos una función para limpiar los datos.
def clean_data(path_to_data:str=None, output_path:str=None) -> None:
    """
    Función para limpiar los datos de las noticias de Newtral.es
    """
    
    assert path_to_data is not None, "Por favor, ingrese la ruta del archivo de datos."
    assert os.path.exists(path_to_data), "La ruta del archivo de datos no existe."

    if output_path is None:
        os.mkdir("data")
        output_path = "data/newtral_es_links_clean.json"

    # Cargamos los datos
    df = pd.read_json(path_to_data, lines=True)

    # Limpiamos los datos
    df = df.dropna() # Eliminamos los valores nulos

    # Guardamos los datos limpios
    df.to_json(output_path, orient='records', lines=True, force_ascii=False)

# Definimos el main() para ejecutar el código
async def main() -> None:
    async with async_playwright() as playwright:
        links = await get_links(playwright) # Obtenemos los links de las noticias
        df = pd.DataFrame(links, columns=["Links"]) # Creamos un DataFrame con los links de las noticias
        df.to_json("data/newtral_es_links.json", orient='records', lines=True, force_ascii=False) # Guardamos los links de las noticias en un archivo JSON

    # Limpiamos los datos
    clean_data(path_to_data="data/newtral_es_links.json", output_path="data/newtral_es_links_clean.json")

# Ejecutamos el main()
asyncio.run(main())