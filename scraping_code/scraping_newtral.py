# Python 3.11
# Author: Christian Badillo
# Date: 2024-16-04

# Importamos las librerias necesarias
import asyncio
from playwright.async_api import async_playwright, Playwright
import pandas as pd
import re
import sys
import os
import time
from typing import List, Tuple, Dict

# Cargamos los datos.
data_path = "/home/chris_badillo/Desktop/scrapping-bolos/data/newtral_es_links_clean.json"
df = pd.read_json(data_path, lines=True)

# Definimos una función para obtener los datos de las noticias.
async def scraping(playwright: Playwright, links:List[str]) -> Tuple[pd.DataFrame, List[str]]:
    """
    Función para realizar el scraping de las noticias de Newtral.es
    """

    # Inicializamos el diccionario para guardar los datos
    data = {"Author": [], 
            "Date": [], 
            "Title": [],
            "Link": [],
            "Content": []}
    
    i = 1 # Contador
    size = pd.Series(links).size # Tamaño de la lista de links
    to_eliminate_sources = [] # Lista para guardar los links que no se pudieron eliminar las fuentes
    separator = "-"*50 # Separador

    # Inicializamos el browser
    browser = await playwright.chromium.launch()

    # Para cada link, obtenemos la información de la noticia
    for link in links:

        # Creamos una nueva pestaña
        page = await browser.new_page()
        timeout_en_milisegundos = 360000
        await page.goto(link, timeout=timeout_en_milisegundos)
        
        # Obtenemos la información de la noticia
        title = await page.locator("h1.entry-title.c-article__title").inner_text()
        date = await page.get_by_role("time").inner_text()
        author = await page.locator("a.author.vcard").nth(0).inner_text()
        content = await page.locator("div.entry-content").inner_text()

        # Progreso
        print(f"Scraping... \nTitle: {title} \nLink: {link}")

        # Eliminamos el "Por " del autor
        author = author.replace("Por ", "")

        # Limpiamos el contenido
        features = {"author": author, "date": date}
        content, to_eliminate_sources = await clean_content(content, page, features, link, to_eliminate_sources)

        # Guardamos la información
        data["Author"].append(author)
        data["Date"].append(date)
        data["Title"].append(title)
        data["Link"].append(link)
        data["Content"].append(content.strip())
        
        # Cerramos la pestaña
        await page.close()

        # Imprimimos Progreso
        print("Completed")
        print(f"Progreso {round(i / size * 100, 2)} %")
        i += 1
        print(separator)

    return pd.DataFrame(data), to_eliminate_sources

# Definimos una función para limpiar el contenido de la noticia

async def clean_content(content: str, page, features: Dict[str, str], link: str, to_eliminate_sources: List[str]) -> Tuple[str, List[str]]:
    """
    Function to clean the content of the news.
    """

    # Eliminamos el "Por autor" del contenido
    author, date = features["author"], features["date"]
    content = content.replace(f"Por {author}", "")

    # Eliminamos la fecha del contenido
    content =  content.replace(date, "")

    # Eliminamos las nuevas lineas
    content =  content.replace("\n", " ")

    # Eliminamos el tiempo de lectura
    expresion = r" *\| <? \d* min lectura"
    content =  re.sub(expresion, "", content)
    expresion = r" *\| \d* min lectura"
    content =  re.sub(expresion, "", content)

    # Eliminamos los tags de los temas relacionados
    tags = await page.locator("ul.tags-links.c-pills.uk-visible\@m").inner_text()
    tags = [tag for tag in tags.split("\n") if tag != ""]
    for tag in tags:
        content =  content.replace(tag, "")
    
    #Eliminamos los espacios en blanco al inicio y al final
    content =  content.strip()

    # Eliminamos las fuentes
    try:
        fuentes = await page.locator("section.o-section--fact-check.c-sources.o-section").inner_text()
        fuentes = [fuente for fuente in fuentes.split("\n") if fuente != ""]
        for fuente in fuentes:
            content = content.replace(fuente, "")
    except:
        to_eliminate_sources.append(link)
        pass

    # Eliminamos lo de la metodología.
    meto_text = await page.locator("section.o-section--fact-check.u-rounded.c-ratings").inner_text()
    meto_text = [meto for meto in meto_text.split("\n") if meto != ""]
    for meto in meto_text:
        content = content.replace(meto, "")
    
    return content, to_eliminate_sources

# Definimos una función para realizar el scraping por lotes
async def scraping_by_batch(playwright: Playwright, links:List[str], batch_num:int=5) -> None:

    """
    Realiza el scraping dividiendo las tareas en lotes.
    """
    size: int = len(links) # Tamaño de la lista de links
    batch_size: int = size // batch_num # Tamaño de los lotes
    lista_links = [links[i:i+batch_size] for i in range(0, size, batch_size)] # Dividimos los links en lotes
    ## Noticias donde hace falta eliminar las fuentes
    sources = [] # Lista para guardar los links que no se pudieron eliminar las fuentes

    # Guardamos los datos por batch
    for i, batch in enumerate(lista_links):

        if i <= 4:
            continue

        sys.stdout.write(f"Starting batch {i} \n") # Imprimimos el progreso

        data, sources_noteliminated = await scraping(playwright, batch) # Realizamos el scraping
        sources.append(sources_noteliminated) # Guardamos los links que no se pudieron eliminar las fuentes
        
        data.to_json(f"/home/chris_badillo/Desktop/scrapping-bolos/data/newtral_es_data_batch_{i}.json", orient='records', lines=True, force_ascii=False) # Guardamos los datos

        sys.stdout.write(f"Batch {i} completed \n") # Imprimimos el progreso
        sys.stdout.write("-"*50)

    # Guardamos los links que no se pudieron eliminar las fuentes
    df_sources = pd.DataFrame(sources)
    df_sources.to_json("/home/chris_badillo/Desktop/scrapping-bolos/data/to_eliminate_sources.json", orient='records', lines=True, force_ascii=False)

async def merge_data(data_path:str, outdir:str, name:str) -> None:
    """
    Une los datos de las noticias en un solo archivo.
    """

    # Obtenemos los archivos
    files = os.listdir(data_path)
    re_expression = r"newtral_es_data_batch_\d+.json" # Expresión regular para leer los archivos
    files_toread = re.compile(re_expression) # Buscamos los archivos que coincidan con la expresión regular

    # cambiamos el directorio
    os.chdir(data_path)

    # Leemos los archivos
    data = pd.concat([pd.read_json(file, lines=True) for file in files if files_toread.match(file)])

    # Guardamos los datos
    data.to_json(os.path.join(outdir, name), orient='records', lines=True, force_ascii=False)

# Inicializamos el loop de asyncio
async def main() -> None:
    async with async_playwright() as playwright:
        # Obtenemos los links
        links = df["Links"]
        # Realizamos el scraping por lotes
        await scraping_by_batch(playwright, links)

        # Unimos los datos
        data_path = "/home/chris_badillo/Desktop/scrapping-bolos/data"
        outdir = "/home/chris_badillo/Desktop/scrapping-bolos/data"
        name = "newtral_es_data.json"
        await merge_data(data_path=data_path, outdir=outdir, name=name)

# Ejecutamos el código
asyncio.run(main())